import EInput from './Input';
import EButton from './Button';
import EDivider from './Divider';
import EIcon from './Icon';

// eslint-disable-next-line prefer-const
let components = [
    EInput,
    EButton,
    EDivider,
    EIcon,
];

const install = (Vue) => {
    /* istanbul ignore if */
    if (install.installed) return;

    components.map(component => Vue.component(component.name, component));
};

/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}

export default {
    install,
    EInput,
    EButton,
    EDivider,
    EIcon,
};
