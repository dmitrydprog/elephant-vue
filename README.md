# Установка проекта.
1. Склонировать репозиторий.

2. В package.json файле куда нужно подключить библиотеку компонентов
установить зависимость:
```json
"elephant-vue": "file://../elephant-vue"
```
> Возможно будут ошибки связанные с импортом. Для исправления
нужно перейти в директорию `elephant-vue` и выполнить команду `npm install`, предварительно удалив папку `node_modules` если существует.

3. Выполнить команду: `npm install` в корне проекта.

4. Добавить css стили elepahnt-а на страницу:
```js
import '@/assets/css/elephant.min.css';
```

5. Подключить плагин библиотеки используя Vue.use:
```js
import ElephantUI from 'elephant-vue/components';
Vue.use(ElephantUI);
```